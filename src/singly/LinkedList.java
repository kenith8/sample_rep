
package singly;

public class LinkedList implements List{

    private Node head;
    private Node tail;
    private int size;
    private int pos = 1;

    public LinkedList(){
        this.head = null;
        this.tail = null;
        this.size = 0;
    }
    
    
    public void addFirst(int data) {
        if(head == null){
            this.head = new Node(data,head);
            this.tail = this.head;
            size++;
        }else{
            this.head = new Node(data,head);
            size++;
        }
    }

   
    public void addLast(int data) {
        if(head == null){
            addFirst(data);
            return;
        }
        this.tail.next = new Node(data,null);
        this.tail = this.tail.next;
        size++;
    }
    
    
    public void splitAt(LinkedList otherList, int data){
        Node tmp = head;
        Node second;
        while(tmp != null){
            if(tmp.data == data){
                
                second = tmp.next;
                
                while(second != null){
                    otherList.addLast(second.data);
                    second = second.next;
                }
                
                tmp.next = null;
                tail = tmp;
            }
            tmp = tmp.next;
        }
    }
    
    public void printList(){
        Node tmp = head;
        while(tmp != null){
            if(tmp.next == null){
                System.out.print(tmp.data);
                System.out.println();
            }
            else{
                System.out.print(tmp.data+", ");
            }
            tmp = tmp.next;
        }
    }
    
    private class Node{
        private int data;
        private Node next;
        
        public Node(int data,Node next){
            this.data = data;
            this.next = next;
        }
    }
}
