
package singly;

import java.util.*;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        LinkedList myList = new LinkedList();
        LinkedList otherList = new LinkedList();

        char choice;
        do{
            System.out.print("Enter number to add: ");
            int data = in.nextInt();
            myList.addLast(data);
            
            System.out.print("Contents: ");
            myList.printList();
            
            System.out.print("Add another number?(y/n): ");
            choice = in.next().charAt(0);
        }while(choice != 'n');
        
        
        
        System.out.print("Split at: ");
        int split = in.nextInt();
        myList.splitAt(otherList, split);
        System.out.println("List 1: ");
        System.out.print("Contents: ");
        myList.printList();
        System.out.println("List 2: ");
        System.out.print("Contents: ");
        otherList.printList();
        
        System.out.print("Add data to which list?(1 or 2): ");
        int list = in.nextInt();
        
        char choice2;
        do{
            switch(list){
                case 1:{
                    System.out.print("Enter number to add: ");
                    int data = in.nextInt();
                    myList.addLast(data);
                    
                    System.out.print("Contents: ");
                    myList.printList();
                }break;
                case 2:{
                    System.out.print("Enter number to add: ");
                    int data = in.nextInt();
                    otherList.addLast(data);
                    
                    System.out.print("Contents: ");
                    otherList.printList();
                }break;
            }
            
            System.out.print("Add another number?(y/n): ");
            choice2 = in.next().charAt(0);
        }while(choice2 != 'n');
    }
}
